var productosObtenidos;

function getProductos() {
    var url ="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(){
      //estatus 4 que crague la información
      if(this.readyState == 4 && this.status ==200){
        //console.log(request.responseText);
        productosObtenidos = request.responseText;
// invocando una función
        procesarProductos();
      }
    }
    request.open("GET",url,true);
    request.send();

}

function procesarProductos(){
  var JSONProductos = JSON.parse(productosObtenidos);
  // aquí nos da el nombre del producto en la posición 0

  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("tablaProductos");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i< JSONProductos.value.length; i++){
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ProductName;
    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;
    var columnaStock = document.createElement("td");
    columnaStock.innerText = JSONProductos.value[i].UnitsInStock;
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);
    tbody.appendChild(nuevaFila);

  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
