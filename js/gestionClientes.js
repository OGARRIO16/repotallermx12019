var clientesObtenidos;
function getClientes() {
    var url ="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(){
      //estatus 4 que crague la información
      if(this.readyState == 4 && this.status ==200){
        //console.log(request.responseText);
        clientesObtenidos = request.responseText;
        //invocando a la función
        ProcesarClientes();

      }
    }
    request.open("GET",url,true);
    request.send();

}

function ProcesarClientes(){
  var rutaBandera= "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var JSONClientes = JSON.parse(clientesObtenidos);
  var divTabla = document.getElementById("tablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i< JSONClientes.value.length; i++){
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;
    var columnaPais = document.createElement("td");
    columnaPais.innerText = JSONClientes.value[i].Country;

    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");

    if (JSONClientes.value[i].Country =="UK"){
      imgBandera.src = rutaBandera + "United-Kingdom.png";

    }else{
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    }

    columnaBandera.appendChild(imgBandera);



    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPais);
    nuevaFila.appendChild(columnaBandera);
    tbody.appendChild(nuevaFila);

  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
